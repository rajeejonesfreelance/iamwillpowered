//
//  IAMUserAuthDataController.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/15/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import Foundation
import Firebase

let kUserType = "User Type"

enum UserType {
    case Client, Trainer
    
    func getUserType() -> String {
        switch self {
        case Client:
            return "Client"
        case Trainer:
            return "Trainer"
        }
    }
    
}


class IAMUserAuthDataController {
    
    /**
     Creates a new user with email and password
     - parameter email: The user email
     - parameter password: The user password
     */
    class func createUser(withEmail email:String!, password:String!) {
        
        rootReference.createUser(email, password: password) {
            (error, result) -> Void in
            
            if error != nil {
                //There was an error creating the account
            } else {
                print("User Created!")
                rootReference.authUser(email, password: password, withCompletionBlock: { (error, authData:FAuthData!) -> Void in
                    if error == nil {
                        print(authData)
                    }
                })
            }
        }
        
    }
    
    class func authenticateUser(withEmail email:String!,password:String!) {
        rootReference.authUser(email, password: password) { (error, authData) -> Void in
            if error == nil {
                print("User Authenticated!")
                print(authData)
            } else {
                print("Invalid User")
            }
        }
    }

}