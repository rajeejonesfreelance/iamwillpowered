//
//  IAMHomeViewController.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/9/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class IAMHomeViewController: UIViewController {
    
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var logoMainTextLabel: UILabel!
    
    @IBOutlet weak var logoTaglineTextLabel: UILabel!
    
    var facebookName:String?
    var facebookData:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = backgroundGradient
        
        configureFacebookButton()

        
        // Do any additional setup after loading the view.
    }

    private func configureFacebookButton() {
        
        facebookButton.addTarget(self, action: "facebookLoginPressed:", forControlEvents:UIControlEvents.TouchUpInside)
    }
    
    func facebookLoginPressed(sender:UIButton!) {
        
        let loginManager = FBSDKLoginManager()
        
        weak var weakSelf = self
        
        loginManager.logInWithReadPermissions(["public_profile"], fromViewController: self) { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            if ((error) != nil) {
                print("Process Error")
            } else if (result.isCancelled) {
                print("Cancelled")
            } else {
                
                //Present next view
                if let _ = result.token {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection:FBSDKGraphRequestConnection!, result, error) -> Void in
                        if (error == nil) {
                            weakSelf?.facebookData = result as? NSDictionary
                            print(weakSelf?.facebookData)
                            weakSelf?.facebookName = weakSelf?.facebookData?.objectForKey("first_name") as? String
                            
                        }
                        weakSelf?.displayLoggedInAlert()
                    })
                }
            }
        }
    
    }

    private func displayLoggedInAlert() {
        
        var displayName = ""
        
        if let facebookName = facebookName {
            displayName = ", " + facebookName
        }
        
        let alertController = UIAlertController(title: "Logged In!", message: "Log in was successful\(displayName)!", preferredStyle: .Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }

    @IBAction func unwindToHomeVC(segue:UIStoryboardSegue) {
        
    }

}
