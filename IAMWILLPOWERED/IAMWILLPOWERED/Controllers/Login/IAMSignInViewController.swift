//
//  IAMSignInViewController.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/13/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import UIKit

class IAMSignInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var seperatorView: UIView!
    
    let loginButton:UIButton = UIButton(type: UIButtonType.Custom)
    let blur = UIVisualEffectView(effect: UIBlurEffect(style:
        UIBlurEffectStyle.Light))
    var keyboardSize:CGRect = CGRectZero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("configureKeyboardSize:"), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("textFieldTextChanged:"), name:UITextFieldTextDidChangeNotification, object: nil)
        
        self.view.backgroundColor = backgroundGradient
        
        usernameTextField.becomeFirstResponder()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        
        loginButton.addSubview(blur)
        
        loginButton.setTitle("Login", forState:UIControlState.Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        loginButton.userInteractionEnabled = true
        
        loginButton.addTarget(self, action: Selector("loginButtonPressed:"), forControlEvents: UIControlEvents.TouchUpInside)
        loginButton.hidden = true

    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func configureKeyboardSize(notification:NSNotification) {
        if let boardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            keyboardSize = boardSize
        }
    }
    
    func adjustLoginButtonFrame() {
        
        loginButton.frame = CGRectMake(0.0, 0.0, keyboardSize.width, 40.0)

        blur.frame = loginButton.bounds
        blur.userInteractionEnabled = false //This allows touches to forward to the button.
    }
    
    func loginButtonPressed(sender:UIButton) {
        print("pressed")
        self.passwordTextField.endEditing(true)
        IAMUserAuthDataController.authenticateUser(withEmail: self.usernameTextField.text, password: self.passwordTextField.text)
//        displayLoggedInAlert()
        
    }
    
    func displayLoggedInAlert() {
        
        let alertController = UIAlertController(title: "Logged In!", message: "Log in was successful!", preferredStyle: .Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        adjustLoginButtonFrame()
        textField.inputAccessoryView = loginButton
    }
    
    func textFieldTextChanged(sender:AnyObject) {
        if usernameTextField.text?.characters.count >= 3 && passwordTextField.text?.characters.count >= 3 {
            loginButton.hidden = false
        } else {
            loginButton.hidden = true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 0 {
            textField.superview?.viewWithTag(1)?.becomeFirstResponder()
        }
        return false
    }
    
    
    @IBAction func unwindToSignInVC(segue:UIStoryboardSegue) {
        
    }

}
