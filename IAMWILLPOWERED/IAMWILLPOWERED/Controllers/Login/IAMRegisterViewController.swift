//
//  IAMRegisterViewController.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/16/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import UIKit

class IAMRegisterViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var verfiyPasswordTextField:UITextField!
    @IBOutlet weak var errorLabel:UILabel!
    
    let registerButton:UIButton = UIButton(type: UIButtonType.Custom)
    let blur = UIVisualEffectView(effect: UIBlurEffect(style:
        UIBlurEffectStyle.Light))
    var keyboardSize:CGRect = CGRectZero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("configureKeyboardSize:"), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("textFieldTextChanged:"), name:UITextFieldTextDidChangeNotification, object: nil)
        
        self.view.backgroundColor = backgroundGradient
        
        usernameTextField.becomeFirstResponder()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        verfiyPasswordTextField.delegate = self
        
        registerButton.addSubview(blur)
        
        registerButton.setTitle("Register", forState:UIControlState.Normal)
        registerButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        registerButton.userInteractionEnabled = true
        
        registerButton.addTarget(self, action: Selector("registerButtonPressed:"), forControlEvents: UIControlEvents.TouchUpInside)
        registerButton.hidden = true
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func configureKeyboardSize(notification:NSNotification) {
        if let boardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            keyboardSize = boardSize
        }
    }
    
    func adjustRegisterButtonFrame() {
        
        registerButton.frame = CGRectMake(0.0, 0.0, keyboardSize.width, 40.0)
        
        blur.frame = registerButton.bounds
        blur.userInteractionEnabled = false //This allows touches to forward to the button.
    }
    
    func registerButtonPressed(sender:UIButton) {
        print("pressed")
        self.verfiyPasswordTextField.endEditing(true)
        
        IAMUserAuthDataController.createUser(withEmail: self.usernameTextField.text, password: self.passwordTextField.text)
        
//        displayLoggedInAlert()
        
    }
    
    func displayLoggedInAlert() {
        
        let alertController = UIAlertController(title: "Logged In!", message: "Log in was successful!", preferredStyle: .Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        adjustRegisterButtonFrame()
        textField.inputAccessoryView = registerButton
    }
    
    func textFieldTextChanged(sender:AnyObject) {
        if usernameTextField.text?.characters.count >= 3 && passwordTextField.text?.characters.count >= 3 && verfiyPasswordTextField.text?.characters.count >= 3 {
            
            if passwordTextField.text == verfiyPasswordTextField.text {
                registerButton.hidden = false
                errorLabel.text = ""
            } else {
                registerButton.hidden = true
                errorLabel.text = "Passwords do not match"
            }
            
        } else {
            registerButton.hidden = true
            errorLabel.text = ""
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.tag == 0 {
            textField.superview?.viewWithTag(1)?.becomeFirstResponder()
        } else if textField.tag == 1 {
            textField.superview?.viewWithTag(2)?.becomeFirstResponder()
        }
        return false
    }
    
    @IBAction func unwindToRegisterVC(segue:UIStoryboardSegue) {
        
    }
    
}
