//
//  Presenter.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/9/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import Foundation

var backgroundGradient: UIColor {
    get{
        if let backgroundImage = UIImage(named: "Background"){
            return UIColor(patternImage: backgroundImage)
        } else {
            return UIColor.clearColor()
        }
    }
}


