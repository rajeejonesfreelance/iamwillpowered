//
//  Constants.swift
//  IAMWILLPOWERED
//
//  Created by Rajeé Jones on 2/6/16.
//  Copyright © 2016 Rajee Jones Freelance. All rights reserved.
//

import Foundation
import Firebase


let firebaseURL = "https://dazzling-inferno-7434.firebaseio.com"
let rootReference = Firebase(url: firebaseURL)
